/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CreatCSV, that } from './creatCSV'
import { getPath, openSync, accessSync,CSVParser,CSVReaderBuilder } from '@ohos/opencsv';
import { CreatSQL } from './creatSQL'
import { Other } from './other'

@Entry
@Component
struct Index {
  private scroller: Scroller= new Scroller()
  @State openser: Array<Array<string>> = [["ID", this.getResourceString($r('app.string.Name')), this.getResourceString($r('app.string.gender')), this.getResourceString($r('app.string.age'))]]
  @State csvName: string= ""
  @State myEnabled: boolean= true
  @State createSqlHide: boolean= true
  @State ids: number= 1
  @State loadingText: string= ""
  @State display: boolean= false
  // private navigating: string[]= ["创建csv", "创建SQL", "其他"]

  loading(text: string) {
    this.loadingText = text
    this.display = true
    setTimeout(() => {
      this.display = false
    }, 3000)
  }

  build() {
    Column() {
      Stack({ alignContent: Alignment.Top }) {
        // 数据显示表格
        GridContainer() {
          Scroll(this.scroller) {
            Grid() {
              ForEach(this.openser, (item: Array<string>, Index) => {
                ForEach(item, (item2:string, Index1) => {
                  if (Index1 == this.openser[0].length - 1 && Index > 0) {
                    GridItem() {
                      Button(item2, { type: ButtonType.Normal })
                        .borderRadius(8)
                        .onClick(() => {
                          this.openser.splice(Index, 1)
                        })
                    }
                    .backgroundColor(0xF9CF93)
                    .width("100%").height(50)
                  } else {
                    GridItem() {
                      Text(item2)
                        .fontSize(18)
                        .textAlign(TextAlign.Center)
                        .width("100%")
                        .height(50)
                    }
                    .backgroundColor(0xF9CF93)
                  }
                })
              })
            }
            .backgroundColor("#e1e1e1")
            .columnsTemplate("1fr ".repeat(this.openser[0].length))
            .columnsGap(2)
            .rowsGap(4)
            .width(400)
            .height(260)
            .margin({ top: 0 })
          }
          .scrollable(ScrollDirection.Horizontal)
        }
        .width(300)
        .alignItems(HorizontalAlign.Start)

        Text(this.loadingText)
          .width(160)
          .height(50)
          .backgroundColor("#efb693")
          .fontSize(14)
          .borderRadius(8)
          .textAlign(TextAlign.Center)
          .margin({ top: 25 })
          .visibility(this.display ? Visibility.Visible : Visibility.None)
      }

      Column() {
        if (this.ids == 1) {
          CreatCSV({ csvName: $csvName, myEnabled: $myEnabled })
        } else if (this.ids == 2) {
          CreatSQL({ myEnabled: $myEnabled, createSqlHide: $createSqlHide, openser: $openser, csvName: $csvName })
        } else if (this.ids == 3) {
          Other({ myEnabled: $myEnabled, csvName: $csvName, openser: $openser })
        }
      }.height(300)

      Button(this.getResourceString($r('app.string.Read_CSV_files_and_display_data')))
        .onClick(() => {
          this.openser.splice(1)
          getPath().then((path) => {
            let hasDir = accessSync(path);
            let hasFile = accessSync(path, this.csvName);
            if (!hasDir || !hasFile) {
              this.loading("The directory or file does not exist!");
              return;
            }
            let rd = openSync(path, this.csvName, 0o2);
            let lines: Array<Array<string>> | null = null;
            let readerBuilder: CSVReaderBuilder = new CSVReaderBuilder(rd);
            let reader = readerBuilder.withCSVParser(new CSVParser()).buildCSVReader();
            lines = reader.readAll();
            for (let i =1; i < lines.length; i++) {
             this.openser.push(lines[i])
            }
            reader.close();
            if (this.openser.length<= 1) {
              this.loading("No data available, please write in first!");
            }
          });
        })
        .enabled(!this.myEnabled)
        .opacity(!this.myEnabled ? 1 : 0.4)
        .backgroundColor("#fd6d6d")
      Row() {
        Button(`${this.getResourceString($r('app.string.last_page'))}`)
          .onClick(() => {
            this.ids--
          })
          .enabled(this.ids > 1)
          .opacity(this.ids > 1 ? 1 : 0.4)
        Button(`${this.getResourceString($r('app.string.next_page'))}`).onClick(() => {
          this.ids++
        })
          .enabled(this.ids < 3)
          .opacity(this.ids < 3 ? 1 : 0.4)
      }
    }
    .width('100%')
    .height('100%')
  }

  getResourceString(res:Resource){
    return getContext().resourceManager.getStringSync(res.id)
  }

  aboutToAppear() {

  }
}
