/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CSVParserBuilder } from './csvParserBuilder';
import { ICSVParser } from './icsvParser';
import { CSVReader } from './csvReader';
import { CSVReaderOptions } from './csvReader';
import { CSVReaderNullFieldIndicator } from './enums/csvReaderNullFiedIndicator';
import { RowValidatorAggregator } from './validators/rowValidatorAggregator';
import { RowValidator } from './validators/rowValidator';
import { LineValidatorAggregator } from './validators/lineValidatorAggregator';
import { LineValidator } from './validators/lineValidator';
import { RowProcessor } from './processor/rowProcessor';
import { getDefaultLocale } from './csvUtils';

export class CSVReaderBuilder {
  private parserBuilder: CSVParserBuilder = new CSVParserBuilder();
  private reader: number;
  private skipLines: number = 0;
  private icsvParser: ICSVParser| null = null;
  private keepCR!: boolean ;
  private verifyReader: boolean = true;
  private nullFieldIndicator: CSVReaderNullFieldIndicator;
  private multilineLimit: number;
  private errorLocale:string;
  private lineValidatorAggregator: LineValidatorAggregator;
  private rowValidatorAggregator: RowValidatorAggregator;
  private rowProcessor: RowProcessor|null;
  constructor(reader: number) {
    this.nullFieldIndicator = CSVReaderNullFieldIndicator.NEITHER;
    this.multilineLimit = 0;
    this.errorLocale = getDefaultLocale();
    this.lineValidatorAggregator = new LineValidatorAggregator();
    this.rowValidatorAggregator = new RowValidatorAggregator();
    this.rowProcessor = null;
    if (reader == null) {
      throw new Error("reader.null");
    } else {
      this.reader = reader;
    }
  }

  protected getReader(): number {
    return this.reader;
  }

  protected getSkipLines(): number {
    return this.skipLines;
  }

  protected getCsvParser(): ICSVParser|null {
    return this.icsvParser;
  }

  protected getMultilineLimit(): number {
    return this.multilineLimit;
  }

  public withSkipLines(skipLines: number): CSVReaderBuilder {
    this.skipLines = Math.max(skipLines, 0);
    return this;
  }

  public withCSVParser(icsvParser: ICSVParser): CSVReaderBuilder {
    this.icsvParser = icsvParser;
    return this;
  }

  public buildCSVReader(): CSVReader {
    let parser: ICSVParser = this.getOrCreateCsvParser();
    return new CSVReader(this.reader,
      new CSVReaderOptions(this.skipLines, parser, this.keepCR, this.verifyReader, this.multilineLimit, this.errorLocale, this.lineValidatorAggregator, this.rowValidatorAggregator, this.rowProcessor));
  }

  public withKeepCarriageReturn(keepCR: boolean): CSVReaderBuilder {
    this.keepCR = keepCR;
    return this;
  }

  protected keepCarriageReturn(): boolean {
    return this.keepCR;
  }

  protected getOrCreateCsvParser(): ICSVParser {
    if (this.icsvParser != null) {
      return this.icsvParser;
    } else {
      return this.parserBuilder.withFieldAsNull(this.nullFieldIndicator)
        .withErrorLocale(this.errorLocale)
        .buildCSVParser();
    }
  }

  public withVerifyReader(verifyReader: boolean): CSVReaderBuilder{
    this.verifyReader = verifyReader;
    return this;
  }

  public isVerifyReader(): boolean {
    return this.verifyReader;
  }

  public withFieldAsNull(indicator: CSVReaderNullFieldIndicator): CSVReaderBuilder {
    this.nullFieldIndicator = indicator;
    return this;
  }

  public withMultilineLimit(multilineLimit: number): CSVReaderBuilder {
    this.multilineLimit = multilineLimit;
    return this;
  }

  public withErrorLocale(errorLocale:string): CSVReaderBuilder {
    if (errorLocale != null) {
      this.errorLocale = errorLocale;
    } else {
      this.errorLocale = getDefaultLocale();
    }
    return this;
  }

  public getErrorLocale() {
    return this.errorLocale;
  }

  public getLineValidatorAggregator(): LineValidatorAggregator {
    return this.lineValidatorAggregator;
  }

  public getRowValidatorAggregator(): RowValidatorAggregator {
    return this.rowValidatorAggregator;
  }

  public withLineValidator(lineValidator: LineValidator): CSVReaderBuilder {
    this.lineValidatorAggregator.addValidator(lineValidator);
    return this;
  }

  public withRowValidator(rowValidator: RowValidator): CSVReaderBuilder {
    this.rowValidatorAggregator.addValidator(rowValidator);
    return this;
  }

  public withRowProcessor(rowProcessor: RowProcessor): CSVReaderBuilder {
    this.rowProcessor = rowProcessor;
    return this;
  }
}