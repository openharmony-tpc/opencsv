/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CsvRuntimeException } from './csvRuntimeException'

export class CsvBeanIntrospectionException extends CsvRuntimeException {
  private bean!: Object;
  private field!: string;
  constructor(options?: CsvBeanIntrospectionExceptionOption) {
    if (options != null && options.message != null) {
      super(options.message);
    } else {
      super();
    }
    if (options != null) {
      this.bean = options.bean;
      this.field = options.field;
    }
  }

  public getBean(): ESObject {
    return this.bean;
  }

  public getField(): ESObject {
    return this.field;
  }
}

export class CsvBeanIntrospectionExceptionOption {
  public bean: Object;
  public field: string;
  public message: string;
  constructor(message: string, bean: Object, field: string) {
    this.bean = bean;
    this.field = field;
    this.message = message;
  }
}