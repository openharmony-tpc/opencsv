/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CSVReaderNullFieldIndicator } from './enums/csvReaderNullFiedIndicator';
import { CSVParser, CSVParserOptions } from './csvParser';
import { getDefaultLocale } from './csvUtils';

export class CSVParserBuilder {
  private separator: string = ',';
  private quoteChar: string = '"';
  private escapeChar: string = '\\';
  private strictQuotes: boolean = false;
  private ignoreLeadingWhiteSpace: boolean = true;
  private ignoreQuotations: boolean = false;
  private nullFieldIndicator: CSVReaderNullFieldIndicator;
  private errorLocate:string;
  constructor() {
    this.nullFieldIndicator = CSVReaderNullFieldIndicator.NEITHER;
    this.errorLocate = getDefaultLocale();
  }

  public withSeparator(separator: string): CSVParserBuilder {
    this.separator = separator;
    return this;
  }

  public withQuoteChar(quoteChar: string): CSVParserBuilder {
    this.quoteChar = quoteChar;
    return this;
  }

  public withEscapeChar(escapeChar: string): CSVParserBuilder {
    this.escapeChar = escapeChar;
    return this;
  }

  public withStrictQuotes(strictQuotes: boolean): CSVParserBuilder {
    this.strictQuotes = strictQuotes;
    return this;
  }

  public withIgnoreLeadingWhiteSpace(ignoreLeadingWhiteSpace: boolean) {
    this.ignoreLeadingWhiteSpace = ignoreLeadingWhiteSpace;
    return this;
  }

  public withIgnoreQuotations(ignoreQuotations: boolean) {
    this.ignoreQuotations = ignoreQuotations;
    return this;
  }

  public buildCSVParser(): CSVParser {
    let csvParserOption = new CSVParserOptions(this.separator, this.quoteChar, this.escapeChar, this.strictQuotes,
      this.ignoreLeadingWhiteSpace, this.ignoreQuotations, this.nullFieldIndicator, this.errorLocate);
    return new CSVParser(csvParserOption);
  }

  public getSeparator(): string {
    return this.separator;
  }

  public getQuoteChar(): string {
    return this.quoteChar;
  }

  public EscapeChar(): string {
    return this.escapeChar;
  }

  public isStrictQuotes(): boolean {
    return this.strictQuotes;
  }

  public isIgnoreLeadingWhiteSpace(): boolean {
    return this.ignoreLeadingWhiteSpace;
  }

  public isIgnoreQuotations(): boolean {
    return this.ignoreQuotations;
  }

  public withFieldAsNull(fieldIndicator: CSVReaderNullFieldIndicator): CSVParserBuilder {
    this.nullFieldIndicator = fieldIndicator;
    return this;
  }

  public withErrorLocale(errorLocale:string): CSVParserBuilder {
    this.errorLocate = errorLocale;
    return this;
  }

  public getNullFieldIndicator(): CSVReaderNullFieldIndicator {
    return this.nullFieldIndicator;
  }
}