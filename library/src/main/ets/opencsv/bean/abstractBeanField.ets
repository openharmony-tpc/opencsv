/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { BeanField } from './beanField'
import { CsvConverter } from './csvConverter'
import file from '@system.file';
import { FieldAccess } from './fieldAccess'
import { PreAssignmentProcessor } from "./processor/preAssignmentProcessor"
import { PreAssignmentValidator } from './validators/preAssignmentValidator'
import { StringProcessor } from './processor/stringProcessor'
import { StringValidator } from './validators/stringValidator'
import { CsvValidationException } from './../exceptions/csvValidationException'
import { CsvBeanIntrospectionException, CsvBeanIntrospectionExceptionOption
} from './../exceptions/csvBeanIntrospectionException'
import { CsvRequiredFieldEmptyException, CsvRequiredFieldEmptyExceptionOption
} from './../exceptions/CsvRequiredFieldEmptyException'
import { getDefaultLocale, isBlank } from './../csvUtils'

export abstract class AbstractBeanField implements BeanField {
  protected type!: Object;
  protected field!: string;
  protected required: boolean;
  protected errorLocale :string;
  protected converter!: CsvConverter;
  protected fieldAccess!: FieldAccess;
  protected processors!: Array<PreAssignmentProcessor>;
  protected validators!: Array<PreAssignmentValidator>;
  constructor(options: AbstractBeanFieldOption) {
    if (options == null) {
      this.required = false;
      this.errorLocale = getDefaultLocale();
    } else {
      this.type = options.type;
      this.field = options.field;
      this.required = options.required == null ? false : options.required;
      this.errorLocale = options.errorLocale == null ? getDefaultLocale() : options.errorLocale;
      this.converter = options.converter;
      this.fieldAccess = new FieldAccess(this.field);
      this.processors = options.processors;
      this.validators = options.validators;
    }
  }

  public getType(): Object {
    return this.type;
  }

  public setType(type: Object) {
    this.type = type;

  }

  public setField(field: string) {
    this.field = field;
    this.fieldAccess = new FieldAccess(this.field);
  }

  public getField(): string {
    return this.field;
  }

  public isRequired(): boolean {
    return this.required;
  }

  public setRequired(required: boolean) {
    this.required = required;
  }

  public setErrorLocale(errorLocale:string) {
    this.errorLocale = errorLocale == null ? getDefaultLocale() : errorLocale
    if (this.converter != null) {
      this.converter.setErrorLocale(this.errorLocale);
    }
  }

  public getErrorLocale() :string {
    return this.errorLocale;
  }

  public setFieldValue(bean: Object, value: string, header: string) {
    if (this.required && isBlank(value)) {
       new CsvRequiredFieldEmptyException(new CsvRequiredFieldEmptyExceptionOption(this.type, [this.field], 'required.field.empty ' + this.field))
    } else {
      let processors: PreAssignmentProcessor[] = this.processors == null ? [] : this.processors;
      let fieldValue: string = value;
      let len: number = processors.length;
      for (let i = 0; i < len; i++) {
        let processor: PreAssignmentProcessor = processors[i]
        fieldValue = this.preProcessValue(processor, fieldValue)

      }
      let validators: PreAssignmentValidator[] = this.validators == null ? [] : this.validators;
      len = validators.length
      for (let i = 0; i < len; i++) {
        let validator: PreAssignmentValidator = validators[i];
        this.validateValue(validator, fieldValue);
      }
      this.assignValueToField(bean, this.convert(fieldValue), header);

    }

  }

  private preProcessValue(processor: PreAssignmentProcessor, value: string): string {
    try {
      let stringProcessor: StringProcessor = processor.processor();
      stringProcessor.setParameterString(processor.paramString());
      return stringProcessor.processString(value);
    } catch (error) {
      throw new CsvValidationException("validator.instantiation.impossible" + this.field);
    }
  }

  private validateValue(validator: PreAssignmentValidator, value: string) {
    try {
      let stringValidator: StringValidator = validator.validator();
      stringValidator.setParameterString(validator.paramString());
      stringValidator.validate(value, this);
    } catch (error) {
      throw new CsvValidationException("validator.instantiation.impossible" + this.field);
    }
  }

  public getFieldValue(bean: Object): Object {
    let o: Object |null= null;

    try {
      o = this.fieldAccess.getField(bean);
      return o;
    } catch (error) {
      let csve: CsvBeanIntrospectionException = new CsvBeanIntrospectionException(new CsvBeanIntrospectionExceptionOption('CsvBeanIntrospectionException', bean, this.field))
      throw csve;
    }
  }

  public indexAndSplitMultivaluedField(value: Object, index: Object): Object[] {
    return [value];
  }

  protected isFieldEmptyForWrite(value: Object): boolean {
    return value == null;
  }

  protected assignValueToField(bean: Object, obj: Object, header: string) {
    if (obj != null) {
      try {
        this.fieldAccess.setField(bean, obj);
      } catch (error) {
        let csve: CsvBeanIntrospectionException = new CsvBeanIntrospectionException(new CsvBeanIntrospectionExceptionOption('CsvBeanIntrospectionException', bean, this.field))
        throw csve;
      }
    }
  }

  protected abstract convert(str: string): Object

  public write(bean: Object, index: Object): ESObject {
    let value: Object | null = bean != null ? this.getFieldValue(bean) : null;
    if (this.required && (bean == null || this.isFieldEmptyForWrite(value as object))) {
     new CsvRequiredFieldEmptyException(new CsvRequiredFieldEmptyExceptionOption(this.type, [this.field], "required.field.empty" + this.field));
    } else {
      let multivalues: Object[] = this.indexAndSplitMultivaluedField(value as object, index);
      let intermediateResult: string[] = new Array<string>(multivalues.length);

      try {
        for (let i = 0; i < multivalues.length; ++i) {
          intermediateResult[i] = this.convertToWrite(multivalues[i]);
        }

        return intermediateResult;
      } catch (error) {
         new CsvRequiredFieldEmptyException(new CsvRequiredFieldEmptyExceptionOption(this.type, [this.field], 'required.field.empty ' + this.field))
      }
    }
  }

  protected convertToWrite(value: Object): string {
    return value != null ? value.toString() : "";
  }
}

export class AbstractBeanFieldOption {
  public type: Object;
  public field: string;
  public required: boolean;
  public errorLocale:string;
  public converter: CsvConverter;
  public processors: Array<PreAssignmentProcessor>;
  public validators: Array<PreAssignmentValidator>;
  constructor(type: Object, field: string, required: boolean, errorLocale:string, converter: CsvConverter, processors: Array<PreAssignmentProcessor>, validators: Array<PreAssignmentValidator>) {
    this.type = type;
    this.field = field;
    this.required = required;
    this.errorLocale = errorLocale;
    this.converter = converter;
    this.processors = processors;
    this.validators = validators;
  }
}