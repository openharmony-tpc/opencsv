# OpenCSV
## OpenCSV简介
   现在好多的网站中导出的文件会出现一种csv文件，接下来学习一下csv文件的导出方式。

CSV文件：Comma-Separated Values，中文叫逗号分隔值或者字符分割值，其文件以纯文本的形式存储表格数据。该文件是一个字符序列，可以由任意数目的记录组成，记录间以某种换行符分割。每条记录由字段组成，字段间的分隔符是其它字符或者字符串。所有的记录都有完全相同的字段序列，相当于一个结构化表的纯文本形式。
用文本文件、excel或者类似与文本文件的编辑器都可以打开CSV文件。

## 下载安装
```shell
ohpm install @ohos/opencsv
```
OpenHarmony ohpm 环境配置等更多内容，请参考[如何安装 OpenHarmony ohpm 包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md)

## 使用说明
写入数据
```javascript
import { getPath, openSync,CSVWriter } from '@ohos/opencsv';
getPath().then((path) => {
   let fd = openSync(path,'test.csv'/* csv文件名 */,0o2 | 0o100, 0o666);
   let writer = new CSVWriter(fd);
   writer.writeNext([1,'张三',18], true); // 写入数据
   writer.writeNext([2,'李四',19], true);
   writer.writeNext([3,'王五',20], true);
   writer.writeNext([4,'赵六',21], true);
   writer.close(); // 写入关闭
})
```

读取数据

```typescript
import { getPath, openSync,CSVReaderBuilder,CSVParser } from '@ohos/opencsv';
getPath().then((path) => {
    let rd = openSync(path, 'test.csv'/* csv文件名 */, 0o2)
    let readerBuilder: CSVReaderBuilder = new CSVReaderBuilder(rd)
    let readerbuildcsv = readerBuilder
          .withCSVParser(new CSVParser())
          .buildCSVReader()
    let lines: Array<Array<string>> = null;
    lines = readerbuildcsv.readAll() // 读取csv文件中所有数据
    console.log(lines)
    /* [
     *  [1,'张三',18]
     *  [2,'李四',19]
     *  [3,'王五',20]
     *  [4,'赵六',21]
     *               ]
     */
    readerbuildcsv.close() //读取关闭
})
```
## 接口说明


1.写入一条数据

```async writeNext(nextLine: Array<string>, appleQuotesToAll: boolean): Promise<void>;```

| 参数名         | 类型              | 必填 | 说明 | 返回值 |
| -------------- |-----------------| ---- |----|-----|
| nextLine | Array< string > | 是   | 写入一条数据 |  无  |

2.写入多条数据

```writeAll(allLines: Array<Array<string>>, appleQuotesToAll: boolean): void;```

| 参数名         | 类型                     | 必填 | 说明 | 返回值 |
| -------------- |------------------------| ---- |----|-----|
| allLines | Array<Array< string >> | 是   | 写入多条数据 |  无  |

3.读取下一条数据

```readNext(): Array<string>```

| 参数名 | 类型  | 必填  | 说明 | 返回值             |
|-----|-----|-----|----|-----------------|
| 无   | 无   | 否   | 读取下一条数据 | Array< string > |

4.读取所有数据

```readAll(): Array<Array<string>>```

| 参数名 | 类型  | 必填  | 说明     | 返回值                    |
|-----|-----|-----|--------|------------------------|
| 无   | 无   | 否   | 读取多条数据 | Array<Array< string >> |

## 约束与限制
在下述版本验证通过：

DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 Release (5.0.0.66)

DevEco Studio (4.1.1.500) --SDK:API11 (4.1.7.3)


## 目录结构
````
|---- opencsv 
|     |---- entry  # 示例代码文件夹
|     |---- library  # opencsv库文件夹
|           |---- Index.ets  # 对外接口
|     |---- README.md  # 安装使用方法                    
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-tpc/opencsv/issues) ，当然，也非常欢迎发 [PR](https://gitee.com/openharmony-tpc/opencsv/pulls) 共建。

## 开源协议
本项目基于 [Apache License 2.0](https://gitee.com/openharmony-tpc/opencsv/blob/master/LICENSE) ，请自由地享受和参与开源。