# OpenCSV
## Introduction
Comma-Separated Values (CSV) files are commonly used in websites. OpenCSV provides APIs to access CSV files.

A CSV file stores table data in plain text. It is a character sequence that consists of records separated by a newline character. Each record consists of fields, which are separated by characters or strings. All records have the same sequence of fields, that is, a structured table in plain text.
You can open a CSV file in text, Excel, or any other editor like text.

## How to Install
```shell
ohpm install @ohos/opencsv
```
For details, see [Installing an OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md).

## How to Use
Writing Data
```javascript
import { getPath, openSync,CSVWriter } from '@ohos/opencsv';
getPath().then((path) => {
   let fd = openSync (path,'test.csv'/* csv filename */,0o2 | 0o100,0o666);
   let writer = new CSVWriter(fd);
   writer.writeNext([1,'Zhang San',18],true); // Write data.
   writer.writeNext ([2,'Li Si',19], true);
   writer.writeNext ([3,'Wang Wu',20], true);
   writer.writeNext ([4,'Zhao Liu',21], true);
   writer.close(); // Close writing.
})
```

Reading Data

```typescript
import { getPath, openSync,CSVReaderBuilder,CSVParser } from '@ohos/opencsv';
getPath().then((path) => {
    let rd = openSync (path,'test.csv'/* csv filename */,0o2)
    let readerBuilder: CSVReaderBuilder = new CSVReaderBuilder(rd)
    let readerbuildcsv = readerBuilder
          .withCSVParser(new CSVParser())
          .buildCSVReader()
    let lines: Array<Array<string>> = null;
    lines = readerbuildcsv.readAll() // Read all data in the CSV file.
    console.log(lines)
    /* [
     *  [1,'Zhang San',18]
     *  [2,'Li Si',19]
     *  [3,'Wang Wu', 20]
     *  [4,'Zhao Liu', 21]
     *               ]
     */
    readerbuildcsv.close () // Close reading.
})
```
## Available APIs


1. Write a data record.

`async writeNext(nextLine: Array<string>, appleQuotesToAll: boolean): Promise<void>;`

| Parameter        | Type             | Mandatory| Description| Return Value|
| -------------- |-----------------| ---- |----|-----|
| nextLine | Array< string > | Yes  | Data record to write.|  This API returns no value. |

2. Write multiple data records.

`writeAll(allLines: Array<Array<string>>, appleQuotesToAll: boolean): void;`

| Parameter        | Type                    | Mandatory| Description| Return Value|
| -------------- |------------------------| ---- |----|-----|
| allLines | Array<Array< string >> | Yes  | Data records to write.|  This API returns no value. |

3. Read the next data record.

`readNext(): Array<string>`

| Parameter| Type | Mandatory | Description| Return Value            |
|-----|-----|-----|----|-----------------|
| None  | None  | No  | Next data record read.| Array< string > |

4. Read all data records.

`readAll(): Array<Array<string>>`

| Parameter| Type | Mandatory | Description    | Return Value                   |
|-----|-----|-----|--------|------------------------|
| None  | None  | No  | All data records read.| Array<Array< string >> |

## Constraints
OpenCSV has been verified only in the following version:

DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 Release (5.0.0.66)

DevEco Studio (4.1.1.500) --SDK:API11 (4.1.7.3)


## Directory Structure
````
|---- opencsv 
|     |---- entry  # Sample code
|     |---- library # OpenCSV library
|           |---- Index.ets  # External APIs
|     |---- README.md  # Brief introduction of the opencsv library                   
````

## How to Contribute
If you find any problem during the use, submit an [Issue](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/issues) or [PR](https://gitee.com/openharmony-tpc/openharmony_tpc_samples) to us.

## License
This project is licensed under [Apache License 2.0](https://gitee.com/openharmony-tpc/opencsv/blob/master/LICENSE).
